package test;

import entree.*;

import java.util.ArrayList;
import java.util.List;

public class TestPersonne {
    public static void main(String[] args) {
        Société S1 = new Société("Bein Sport", "Capitaliste");
        Personne conjoint = null;
        List<String> prénoms = new ArrayList<>();
        prénoms.add("Louka");
        prénoms.add("Andre");
        Personne p1 = new Personne("Lemarchand",prénoms, Genre.HOMME, conjoint,  S1, "Employé");
        System.out.println(p1.getPrenom());
        System.out.println(p1.getNom());
        p1.setNom("LesMarchands");
        System.out.println(p1.toString(Presentation.SIMPLE, Sens.NOM_PRENOMS));
    }
}
