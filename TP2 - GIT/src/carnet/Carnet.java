package carnet;

import entree.*;

import java.util.ArrayList;
import java.util.LinkedList;

public class Carnet {
    private static int indSoc = 0;
    private static int indPers = 0;
    private static ArrayList<Société> sociétéList;
    private static ArrayList<Personne> personneList;
    public Carnet(){
        this.sociétéList = new ArrayList<>();
        this.personneList = new ArrayList<>();
    }
    public Personne getPersonneByID(int IDPers){
        return personneList.get(IDPers);
    }
    public Société getSocieteByID(int IDSoc){
        return sociétéList.get(IDSoc);
    }
    public void AjSCarnet (Société société){
        this.sociétéList.add(société);
    }
    public void AjPCarnet (Personne personne){
        this.personneList.add(personne);
    }
    public void affichageCarnet(Ordre ordre, Presentation presentation, Sens sens){
        System.out.println("Sociétés :");
        if (ordre == Ordre.CROISSANT) {
            for (Société s : sociétéList) {
                System.out.println(s.toString());
            }
            System.out.println("");
            System.out.println("Personnes");
            for (Personne p : personneList) {
                System.out.println(p.toString(presentation, sens));
            }
        } else if (ordre == Ordre.DECROISSANT){
            for (int soc = sociétéList.size(); soc >= 0; soc--){
                System.out.println(sociétéList.get(soc));
            }
            System.out.println("");
            System.out.println("Personnes");
            for (int pers = personneList.size(); pers >= 0; pers--){
                System.out.println(personneList.get(pers));
            }
        }
    }
    public void ajoutEntree(){}
}
