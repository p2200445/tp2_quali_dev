import carnet.Carnet;
import entree.Genre;
import entree.Personne;
import entree.Société;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class lectureFichier {
    String delimiter =";";
    String delimiter2 =",";
    Carnet carnet = new Carnet();
    public lectureFichier(String csvFile) throws IOException {
        File file = new File(csvFile);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        String[] tempArr;
        while ((line = br.readLine()) != null) {
            tempArr = line.split(delimiter);
            if (tempArr[1] == "PERSONNE"){
                String ID = tempArr[0];
                List<String> prénoms = new ArrayList<>();
                for (String temp2 : tempArr[2].split(delimiter2)){
                    prénoms.add(temp2);
                }
                String nom = tempArr[3];
                String genre = tempArr[4];
                int id_conjoint = Integer.parseInt(tempArr[5]);
                int id_societe = Integer.parseInt(tempArr[6]);
                String fonction = tempArr[7];
                Personne p = new Personne(nom, prénoms, Genre.valueOf(genre), carnet.getPersonneByID(id_conjoint), carnet.getSocieteByID(id_societe), fonction);
                carnet.AjPCarnet(p);
            } else if (tempArr[1] == "SOCIETE") {

            }
        }
    }
}
