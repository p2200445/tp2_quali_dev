package entree;

import java.util.ArrayList;
import java.util.List;

public class Personne {
    private String nom;
    List<String> prenom = new ArrayList<String>();
    private Genre genre;
    private Personne conjoint;
    private Société société;
    private String fonction;

    public Personne(String nom, List<String> prenom1, Genre genre, Personne conjoint, Société soc, String fonction) {
        this.nom = nom;
        this.prenom = prenom1;
        this.genre = genre;
        this.conjoint = conjoint;
        this.société = soc;
        this.fonction = fonction;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(List<String> prenom) {
        this.prenom = prenom;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Personne getConjoint() {
        return conjoint;
    }

    public void setConjoint(Personne conjoint) {
        this.conjoint = conjoint;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public List<String> getPrenom() {
        return prenom;
    }

    public void recherche() {

    }

    public String toString(Presentation pres, Sens sens) {
        String retour = "";
        if (pres.equals(Presentation.ABREGE)) {
            if (sens.equals(Sens.PRENOMS_NOM)) {
                retour = this.getPrenom() + this.getNom();
            } else {
                retour = this.getNom() + this.getPrenom();
            }
        }
        if (pres.equals(Presentation.SIMPLE)) {
            if (this.genre.equals(Genre.HOMME)) {
                retour += "M.";
            }
            if (this.genre.equals(Genre.FEMME)) {
                retour += "Mme ";
            }
            if (sens.equals(Sens.PRENOMS_NOM)) {
                retour += this.getPrenom() + this.getNom();
            }
            if (sens.equals(Sens.NOM_PRENOMS)) {
                retour += this.getNom() + this.getPrenom();
            }
             if (this.société != null) {
                retour += "(" + this.société + ")";
            }

        }
        if (pres.equals(Presentation.COMPLET)) {
            if (this.genre.equals(Genre.HOMME)) {
                retour += "M.";
            }
            if (this.genre.equals(Genre.FEMME)) {
                retour += "Mme ";
            }
            if (sens.equals(Sens.PRENOMS_NOM)) {
                retour += this.getPrenom() + this.getNom();
            }
            if (sens.equals(Sens.NOM_PRENOMS)) {
                retour += this.getNom() + this.getPrenom();
            }
            if (this.société != null) {
                retour += "\n\t Société : " + this.société;
            }

            if (this.fonction != null) {
                retour += "\n\t Fonction : " + this.fonction;
            }

        }

        return retour;
    }

}
