package entree;

public class Société {
    public String nom;
    public String raisonSociale;
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void recherche(){}
    public String getNom() {
        return nom;
    }
    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }
    public String getRaisonSociale() {
        return raisonSociale;
    }
    public Société(String nom, String raisonSociale){
        this.nom = nom;
        this.raisonSociale = raisonSociale;
    }
    @Override
    public String toString() {
        return getNom()+getRaisonSociale();
    }
}
